const model = require("../models/transactions");
const { v4: uuidv4 } = require("uuid");

const trxType = { CREDIT: "credit", DEBIT: "debit" };

const fetchTrxList = () => model.getTransactions();

const fetchTrxById = (id) => model.getTransactionById(id);

const fetchBalance = () => model.getBalance();

const commitTrx = async (type, amount) => {
  const balance = model.getBalance();
  const newBalance = type === "credit" ? balance + amount : balance - amount;
  if (newBalance < 0) throw new Error("negative balance");
  const newItem = {
    id: uuidv4(),
    type,
    amount,
    effectiveDate: new Date(),
  };
  await model.commitAccountData(newBalance, newItem);
};

module.exports = {
  fetchTrxList,
  fetchTrxById,
  trxType,
  commitTrx,
  fetchBalance,
};
