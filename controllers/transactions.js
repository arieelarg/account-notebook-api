const router = require("express").Router();
const service = require("../services/transactions");

const Ajv = require("ajv");
const ajv = new Ajv({ allErrors: true });

const transactionSchema = {
  type: "object",
  properties: {
    type: { type: "string", enum: [...Object.values(service.trxType)] },
    amount: { type: "number", minimum: 0 },
  },
  required: ["type", "amount"],
};

const transactionId = {
  type: "object",
  properties: {
    trxId: { type: "string" },
  },
  required: ["trxId"],
};

const validateCommit = (req, res, next) =>
  ajv.validate(transactionSchema, req.body)
    ? next()
    : res
        .status(400)
        .json(ajv.errors.map(({ params, message }) => ({ params, message })));

const validateTrxId = (req, res, next) =>
  ajv.validate(transactionId, req.params)
    ? next()
    : res
        .status(400)
        .json(ajv.errors.map(({ params, message }) => ({ params, message })));

const fetchHistory = (req, res) => {
  try {
    const transactions = service.fetchTrxList();
    return transactions.length
      ? res.json([...transactions])
      : res.status(400).json({ error: "empty transactions" });
  } catch (e) {
    return res.status(400).json({ error: "invalid status value" });
  }
};

const getTrxId = (req, res) => {
  const { trxId } = req.params;
  try {
    const transaction = service.fetchTrxById(trxId);
    return transaction
      ? res.json({ ...transaction })
      : res.status(402).json({ error: "transaction not found" });
  } catch (e) {
    return res.status(400).json({ error: "invalid ID supplied" });
  }
};

const fetchBalance = (req, res) => {
  try {
    const balance = service.fetchBalance();
    return res.json({ balance });
  } catch (e) {
    return res.status(400).json({ error: "invalid request" });
  }
};

const commitTrx = async (req, res) => {
  const { type, amount } = req.body;
  try {
    await service.commitTrx(type, amount);
    return res.json({ status: "transaction stored" });
  } catch (e) {
    return res.status(400).json({ error: "invalid operation" });
  }
};

router.get("/", fetchHistory);
router.get("/balance", fetchBalance);
router.get("/:trxId", validateTrxId, getTrxId);
router.post("/", validateCommit, commitTrx);

module.exports = router;
