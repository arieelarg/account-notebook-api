const express = require("express");
const app = express();
const cors = require("cors");
const routes = require("./routes");

require("dotenv").config();

const port = process.env.PORT || 3000;
const host = '0.0.0.0';

app.use(cors());
app.use(express.json());

app.use("/api", routes);

// To keep alive session.
app.get("/", (req, res) => {
  return res.send("Hello");
});

app.listen(port, host, () => console.log(`Listening on port: ${port}`));
