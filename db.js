let accountDb = { balance: 0, transactions: [] };
const db = {};
const getDb = () => db;

db.insert = ({ transaction }) => {
  accountDb.transactions = [...accountDb.transactions, transaction];
};

db.update = ({ balance }) => {
  accountDb.balance = balance;
};

db.get = () => accountDb;

module.exports = { getDb };
