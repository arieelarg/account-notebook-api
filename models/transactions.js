const Mutex = require("async-mutex").Mutex;
const mutex = new Mutex();
const db = require("../db");

const getTransactionById = (transactionId) => {
  const { transactions } = db.getDb().get();
  return transactions.find(({ id }) => id === transactionId);
};

const getTransactions = () => {
  const { transactions } = db.getDb().get();
  return transactions;
};

const getBalance = () => {
  const { balance } = db.getDb().get();
  return balance;
};

const commitAccountData = async (balance, transaction) => {
  const release = await mutex.acquire();
  try {
    db.getDb().update({ balance });
    db.getDb().insert({ transaction });
  } finally {
    release();
  }
};

module.exports = {
  getTransactions,
  getTransactionById,
  getBalance,
  commitAccountData,
};
